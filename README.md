# Demo project

## Installation

1. clone this repository and cd into the folder
2. `npm install --force` (need `force` because of a reactjs version conflict, no biggie)
3. `npm run build`
4. Install the JSON Server - `npm install -g json-server`
5. Start the JSON Server - `json-server --watch db.json -p 3001`
6. Start the application - `npm run start`
7. If the app is started on port `3000`, visit `http://localhost:3000`
8. ...
9. Have fun
