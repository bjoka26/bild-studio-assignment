export type Project = {
    image: string;
    url: string;
    category: number;
    title: string;
    description: string;
};
