export type ServiceType = {
    icon: JSX.Element;
    name: string;
    text: string;
    list: { 
        text: string 
    }[];
};
