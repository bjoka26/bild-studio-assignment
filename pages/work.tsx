import Head from "next/head";
import { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import styled from "styled-components";

import BigHeroTitle from "../components/BigHeroTitle";
import Footer from "../components/Footer";
import ProjectsGridView from "../components/ProjectsGridView";
import ProjectsListView from "../components/ProjectsListView";
import Topbar from "../components/Topbar";
import GridView from "../icons/GridView";
import ListView from "../icons/ListView";
import apiClient from "../lib/ApiClient";
import { Project } from "../types/Project";

const WorkControls = styled.div({
    display: "flex",
    flexDirection: "row",
    flexWrap: "nowrap",
    alignContent: "center",
    justifyContent: "space-between",
    alignItems: "center",
});

const WorkDisplayOptions = styled.div({
    "svg": {
        cursor: "pointer",

        "&:nth-of-type(1)": {
            marginRight: ".8rem",
        },

        "&:hover": {
            "path": {
                fill: "#2ecc71",
            },
        },

        "&.active": {
            "path": {
                fill: "#2ecc71",
            },
        },
    },
});

const Categories = styled.div({
    margin: "3rem 0",
    fontSize: "1.3rem",
    fontWeight: "600",
});

const CategoryItem = styled.div({
    display: "inline-block",
    color: "#8a8888",
    background: "url('./images/slash.png') no-repeat",
    backgroundPosition: "right",
    paddingRight: "1.5rem",
    textTransform: "uppercase",
    userSelect: "none",
    cursor: "pointer",

    "&:nth-last-of-type(1)": {
        background: "transparent",
    },

    "&.active": {
        color: "#2ecc71",
    }
});

export default function Home() {
    const [activeCategory, setActiveCategory] = useState(-1);
    const [viewType, setViewType] = useState(0);
    const [numberOfResults, setNumberOfResults] = useState("?_limit=9");
    const [projects, setProjects] = useState<Project[]>([]);
    const [internalProjects, setInternalProjects] = useState<Project[]>([]);
    const [workCategories, setWorkCategories] = useState<any[]>([]);

    useEffect(() => {
        window.onscroll = function(ev) {
            if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                setNumberOfResults("");
            }
        };
    }, [])

    useEffect(() => {
        apiClient.get(`/projects${numberOfResults}`).then((response) => {
            setProjects(response.data);
            setInternalProjects(response.data);
        });

        apiClient.get("/project_categories").then((response) => {
            setWorkCategories(response.data);
        });
    }, [numberOfResults]);

    const changeCategory = (category: number) => {
        setActiveCategory(category);

        if (category === -1) {
            setInternalProjects(projects);
            return;
        }

        const filteredProjects = projects.filter((project) => {
            return project.category === category;
        });

        setInternalProjects(filteredProjects);
    }

    return (<>
        <Head>
			<title>Display - Work</title>
			<meta name="description" content="Work by Display company" />
			<link rel="icon" href="/favicon.ico" />
		</Head>

		<Topbar />

        <BigHeroTitle>
            <Container>
                Check out what I can do
            </Container>
        </BigHeroTitle>

        <Container className="mb-5">
            <WorkControls>
                <Categories>
                    {
                        workCategories && workCategories.map((category, index) => {
                            return <CategoryItem
                                className={activeCategory === category.id ? 'active' : ''}
                                onClick={() => changeCategory(category.id)}
                                key={`category${index}`}
                            >
                                {category.name}
                            </CategoryItem>
                        })
                    }
                </Categories>

                <WorkDisplayOptions>
                    <GridView className={viewType === 0 ? 'active' : ''} onClick={() => setViewType(0)} />
                    <ListView className={viewType === 1 ? 'active' : ''} onClick={() => setViewType(1)} />
                </WorkDisplayOptions>
            </WorkControls>

            {
                viewType === 0 ?
                    <ProjectsGridView projects={internalProjects} /> : <ProjectsListView projects={internalProjects} />
            }
        </Container>

        <Footer />
    </>);
}
