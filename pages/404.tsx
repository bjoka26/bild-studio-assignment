import Head from "next/head";
import styled from "styled-components";

import Button from "../components/Button";
import Topbar from "../components/Topbar";

const Container = styled.div({
    textAlign: "center",
});

const Error = styled.p({
    margin: "0",
    fontSize: "10rem",
    fontWeight: "bold"
});

const ErrorMessage = styled.p({
    margin: "20px",
    fontSize: "2rem"
});

export default function NotFound() {

    return <>
        <Head>
			<title>Display - 404 Page Not Found</title>
			<meta name="description" content="A 404 page for the portfolio" />
			<link rel="icon" href="/favicon.ico" />
		</Head>

        <Topbar />
        <Container>
            <Error>404</Error>
            <ErrorMessage>Ooops! There is nothing here 🧐</ErrorMessage>
            <a href="/">
                <Button>Go back to safety</Button>
            </a>
        </Container>
    </>;
}
