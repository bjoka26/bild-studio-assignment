import Head from 'next/head'

import FeaturedProjects from "../components/FeaturedProjects"
import Footer from "../components/Footer"
import GetToKnowUs from "../components/GetToKnowUs"
import HeroBanner from "../components/HeroBanner"
import HeroDescription from "../components/HeroDescription"
import Topbar from "../components/Topbar"

export default function Home() {
	return (<>
		<Head>
			<title>Display</title>
			<meta name="description" content="Super cool portfolio site for Display company" />
			<link rel="icon" href="/favicon.ico" />
		</Head>

		<Topbar />

		<HeroBanner />

		<HeroDescription />

		<GetToKnowUs />

		<FeaturedProjects />

		<Footer />
	</>)
}
