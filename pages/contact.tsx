import Head from "next/head";
import { Container } from "react-bootstrap";
import BigHeroTitle from "../components/BigHeroTitle";
import ContactForm from "../components/ContactForm";
import Footer from "../components/Footer";
import styled from "styled-components";

import GoogleMap from "../components/GoogleMap";
import TextAndParagraph from "../components/TextAndParagraph";
import Topbar from "../components/Topbar";

const ContactContainer = styled.div({
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",

    "@media only screen and (max-width: 992px)": {
        flexDirection: "column",
    },
});

const FormContainer = styled.div({
    width: "560px",

    "@media only screen and (max-width: 992px)": {
        width: "100%",
    },
});

const Wrapper = styled.div({
    width: "289px",
    
    ".mail-address": {
        color: "#2ecc71"
    },

    ".contact-info": {
        margin: "20px 0 20px 0"
    },

    "@media only screen and (max-width: 992px)": {
        maxWidth: "100%",
    },
});

const FlexText = styled.p({
    color: "#8a8888",
    margin: "0px",
    display: "flex",
    justifyContent: "space-between",
});

export default () => {

    return <>
        <Head>
			<title>Display - Contact Us</title>
			<meta name="description" content="A page containing contact info" />
			<link rel="icon" href="/favicon.ico" />
		</Head>

        <Topbar />

        <BigHeroTitle>
            <Container>
                Got a question or inquiry
            </Container>
        </BigHeroTitle>

        <GoogleMap />

        <ContactContainer className="container">
            <FormContainer>
                <TextAndParagraph alignText="start" margin="0px">
                    <h4>Contact Form</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt dolor et tristique bibendum. Aenean sollicitudin vitae dolor ut posuere.</p>
                </TextAndParagraph>
                <ContactForm />
            </FormContainer>
                <Wrapper>
                    <TextAndParagraph alignText="start" margin="0px">
                        <h4>Contact Info</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit velit justo.</p>
                        <div className="contact-info">
                            <p><b>email:</b> <a className="mail-address" href="mailto:info@display.com">info@display.com</a></p>
                            <p><b>phone:</b> <span>1.306.222.4545</span></p>
                        </div>
                        <p>222 2nd Ave South <br></br>Saskabush, SK   S7M 1T6</p>
                    </TextAndParagraph>
                    <TextAndParagraph alignText="start" margin="0px">
                        <h4>Store Hours</h4>
                        <FlexText>Monday - Thursday <span>8 am - 5 pm</span></FlexText>
                        <FlexText>Friday<span>8 am - 6 pm</span></FlexText>
                        <FlexText>Saturday<span>9 am - 5 pm</span></FlexText>
                        <FlexText>Sunday & Holidays <span>Closed</span></FlexText>
                    </TextAndParagraph>
                </Wrapper>
        </ContactContainer>

        <Footer style={{marginTop: "5rem"}} />
    </>;
}
