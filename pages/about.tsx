import styled from "styled-components";
import { Container } from "react-bootstrap";
import Head from "next/head";

import aboutImage from "../public/about.png";
import TextAndParagraph from "../components/TextAndParagraph";
import Services from "../components/services/Services";
import Topbar from "../components/Topbar";
import Footer from "../components/Footer";
import BigHeroTitle from "../components/BigHeroTitle";

const PageGrid = styled.div({
    gridArea: "2 / 1 / 2 / 6"
});

const AboutContent = styled.div({
    marginTop: "50px",
    display: "grid",
    gridTemplateColumns: "auto auto auto",
    gridColumnGap: "50px",
    alignItems: "start",
    justifyContent: "center",
    justifyItems: "center",

    "p": {
        marginLeft: "30px",
        color: "#8a8888",
        fontSize: ".875rem"
    },

    "@media only screen and (max-width: 1024px)": {
        gridTemplateColumns: "repeat(1, 1fr)",
    }
});

const ImageText = styled.div({
    marginTop: "20px"
});

const Image = styled.img({
    maxWidth: "380px",
    height: "260px",

    "@media only screen and (max-width: 375px)": {
        width: "250px",
        height: "190px"
    },
});

export default () => {
    return <>
        <Head>
			<title>Display - About Us</title>
			<meta name="description" content="A page containing information about us" />
			<link rel="icon" href="/favicon.ico" />
		</Head>

        <Topbar />

        <PageGrid>
            <BigHeroTitle>
                <Container>
                    About my Business
                </Container>
            </BigHeroTitle>
            <div className="container">
                <AboutContent>
                    <Image src={aboutImage.src} alt="Hard Working man" />
                    <ImageText>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu erat lacus, vel congue mauris. Fusce velit justo, faucibus eu sagittis ac, gravida quis tortor. Suspendisse non urna mi, quis tincidunt eros. Nullam tellus turpis, fringilla sit amet congue ut, luctus a nulla. Donec sit amet sapien neque, id ullamcorper diam. Nulla facilisi. Pellentesque pellentesque arcu a elit congue lacinia.</p>
                        <p>Nullam tellus turpis, fringilla sit amet congue ut, luctus a nulla. Donec sit amet sapien neque, id ullamcorper diam. Nulla facilisi. Pellentesque pellentesque arcu a elit congue lacinia. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu erat lacus, vel congue mauris. Fusce velit justo, faucibus eu sagittis ac, gravida quis tortor. Suspendisse non urna mi, quis tincidunt eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </ImageText>
                </AboutContent>
                <AboutContent>
                    <TextAndParagraph alignText="start" margin="0">
                        <h4>Mission Statement</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu erat lacus, vel congue mauris. Fusce velit justo, faucibus eu sagittis ac, gravida quis tortor. Suspendisse non urna mi, quis tincidunt eros. Nullam tellus turpis, fringilla sit amet congue ut, luctus a nulla. Donec sit amet sapien neque, id ullamcorper diam. Nulla facilisi. Pellentesque pellentesque arcu a elit congue lacinia.</p>
                    </TextAndParagraph>

                    <TextAndParagraph alignText="start" margin="0">
                        <h4>Fun Facts</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu erat lacus, vel congue mauris. Fusce velit justo, faucibus eu sagittis ac, gravida quis tortor. Suspendisse non urna mi, quis tincidunt eros. Nullam tellus turpis, fringilla sit amet congue ut, luctus a nulla. Donec sit amet sapien neque, id ullamcorper diam. Nulla facilisi. Pellentesque pellentesque arcu a elit congue lacinia.</p>
                    </TextAndParagraph>
                </AboutContent>
            </div>
            <Services />
        </PageGrid>

        <Footer />
    </>;
}
