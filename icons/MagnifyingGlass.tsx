import React from "react";

type Props = {
    color?: string;
} & React.SVGAttributes<any>;

const Camera: React.FC<Props> = ({ color, ...props }) => {
    return <svg version="1.2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45 46" width="45" height="46" {...props}>
    <path fillRule="evenodd" fill={color ?? "#737373"} className="service-icon" d="m44.1 44l-1.1 1.1c-1.2 1.2-3.2 1.2-4.4 0l-8.7-8.8c-1-1-1.2-2.5-0.5-3.7l-4.8-4.7c-2.5 1.9-5.7 3-9.2 3-8.5 0-15.4-6.9-15.4-15.4 0-8.6 6.9-15.5 15.4-15.5 8.6 0 15.5 6.9 15.5 15.5 0 3.9-1.5 7.5-4 10.3l4.7 4.7c1.2-0.7 2.7-0.6 3.7 0.4l8.8 8.7c1.2 1.2 1.2 3.2 0 4.4zm-17.1-28.6c0-6.4-5.2-11.6-11.6-11.6-1.8 0-3.4 0.4-4.9 1.2 0.8 0.8 2 2 2 2-1.2 0-5.3 4.1-5.3 5.5 0-0.1-1.3-1.3-2.2-2.1-0.7 1.5-1.2 3.2-1.2 5 0 6.4 5.2 11.6 11.6 11.6 6.4 0 11.6-5.2 11.6-11.6z"/></svg>
}

export default Camera;
