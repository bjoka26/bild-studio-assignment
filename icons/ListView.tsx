import React from "react";

type Props = {
    color?: string;
} & React.SVGAttributes<any>;

const ListView: React.FC<Props> = ({ color, ...props }) => {
	return <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15.031" viewBox="0 0 15 15.031" {...props}>
        <path id="_3" data-name="3" fill={color ?? "#737373"} fillRule="evenodd" d="M1154,341h15v3h-15v-3Zm0-6.031h15V338h-15v-3.031Zm0-6h15V332h-15v-3.031Z" transform="translate(-1154 -328.969)"/>
    </svg>
}

export default ListView;
