import React from "react";

type Props = {
    color?: string;
} & React.SVGAttributes<any>;

const Camera: React.FC<Props> = ({ color, ...props }) => {
    return <svg version="1.2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 43" width="50" height="43" {...props}>
    <path fillRule="evenodd" fill={color ?? "#737373"} className="service-icon" d="m3.6 0h42.8c2 0 3.6 1.6 3.6 3.6v35.8c0 2-1.6 3.6-3.6 3.6h-42.8c-2 0-3.6-1.6-3.6-3.6v-35.8c0-2 1.6-3.6 3.6-3.6zm0.4 39h42v-30h-42zm17-33h25v-3h-25zm-16.5-1.5c0 1 0.8 1.8 1.7 1.8 1 0 1.8-0.8 1.8-1.8 0-1-0.8-1.8-1.8-1.8-0.9 0-1.7 0.8-1.7 1.8zm5.3 0c0 1 0.8 1.8 1.8 1.8 1 0 1.8-0.8 1.8-1.8 0-1-0.8-1.8-1.8-1.8-1 0-1.8 0.8-1.8 1.8zm5.5 0c0 1 0.8 1.8 1.8 1.8 1 0 1.8-0.8 1.8-1.8 0-1-0.8-1.8-1.8-1.8-1 0-1.8 0.8-1.8 1.8z"/></svg>
}

export default Camera;
