import React from "react";

const Logo: React.FC = ({ ...props }) => {
    return <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        width="136"
        height="50"
        viewBox="0 0 136 50"
        {...props}
    >
        <image
            id="logo.PNG"
            width="136"
        height="50"
            xlinkHref="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIgAAAAyCAYAAACH65NBAAAF3ElEQVR4nO2cv0srWxDHx8crFCzMK1TEQnMLBbHw5pZ2Rm1EK19jYaf2Hoh/guL+A9re7gUEQbB4FopoIW+1EARBo4WINm8t/NW9xywzuXPX3ZNNjNmYOx9Ykuyesz+/OzNn5pCmxcXF/6AxaHIcp0EupX747Ve/AYodFYhiRQWiWFGBKFZUIIoVFYhiRQWiWFGBKFZUIIoVFYhiRQWiWFGBKFZ+r/T2tLa2Qm9vr/+9u7s7tM3NzQ08PT1BoVDQp/BJqUggg4ODMDs7G7v96+srXF9fw8XFBezu7jb2HW0wKrYg5dDc3Az9/f3+Mjw8DJubm3B6evqr3/tPQVUEsrq6Cvf392/Wp9Np6Ovrg4GBAejs7PTXtbW1+dZna2tLrcknoCoCCRMHgrEHLtvb275bmpqa8gWCTExMwPPzMxwdHdXFXTLGZAAgRT8LjuP8FDgZY6K6prH9h5/gD7IA8DcA7ADAaNxOlU6mqomLQdClXF1dwcLCQtGaTE5OwtnZGTw+PtbqNH7CGMM3O2wbfhToQeTpM8g0APwFAC4AfEviGj6amg5zUQjfv3/3g1ag2GRsbCyBy34DCmGe3shR+r5CjeZIRMsh/VKBz4ajZhaEQXd0cHAAIyMj/pqvX7/CxsZG0vcVXcp6yPolY0wOAHjx14nt62Q9GnYcn0iibH9/v/gdrQjGJ/WK4zhoSf6k08tRzCFBgXh1ewHvpOYWBMjVYF6kp6fH/42Jtnoe9jqOs2OMWSd3MyesSJriEI+siYS3ZWm7S228QF+X4psMtc0E2pfDNO03Q31cip+KFs4Yw228MKtpjEnRNSLriaXab29vi99bWlqSOo1yyFPbjOiTpthkOrAfvMGXtC1FfYLtuC9v+4f68QNao3UZiMcyBcw52gfv/1I8cGY5IqYCOkfcNuc4jpeYQF5eXorfu7q6kjqNcuBRTKkHlqIb7NHIBpcvtISNhHL0AEepDX7+QYLMkFDikBd9R+m4PAxeJsuA1pAtSsoYExQOkBUDtl5arCufUiMWzqe4tDCFiGA2RaMmKR6P1hVof0ELFYYbIsAdetCpwD7YtWRlYxLRtGyjAqk+LIqsGPnYKAj3JZFxTVw3w2TIKsmgWgqbjzfNloV/83Z0L5CkQGTccXl5mdRplIMM/Gzw2w8iBshZLI9tiMzHiisQPM6/FLus0fGzwUaUJWaRSDfDbYuCTUwgnyTukPDNKyUQoDd/lEx8WgShwSFyNWFBuDQsx1ikKZC3kRStCPzsXgoUp/gkIhCcS8JDXOT8/DyJ0ygXftPiCAREreQL9UlbRg5RsKBKJeKydH4uHTNfKjcjgtUM1aGK7kW2S0Qg3779KFs8PDzU/YQiyqZyUa7c3ERBjCbCgk2b+4jr1rhdWCxjC6plsJoNrPOpeaIMrQfOCWFcN+4LWXvI7C4L6zH/zpMIe6tT5B6C+2ar4EU8+DCCLiwTkgOR5EV+BtvuBKvYNReILPlj0U6m3RMkTZVdJiMynSkReIblMYJwomlduBZ+SCsh7T3anhaZVjkCmo+Rys+LfIonMrM5MVR+A4rBGJOPci9Qa4HMzMzA0NBQ8TcW7ZIq9QdIh5T9Od2dj+PTQ5DxhkfBYphAXFrPmVC5fimmKLkavSYKix7t16VriwqQWSChqfeaCASLcePj48V5IMjJyYk/kShJsMaCkX6pyUIQPWFoh0YKEhYUp9ijEmTB/XBgli1RAAw7Jojjci1HHtP270tsXcLEWx2BhFVjMc+BRTicdiiFgRweHtZDib+I4zgfEQh5Md/+IJX0kcS+FnoxchHFRp+qCCTuDHes4O7t7emE5TqAxMFudYUzp0E+1MVgEHp3d+dnSo+PjyPnriq1xRizJkv6NOcllIoEghYAfXJHRwe0t7dHtlGssAtKYpzPcceSTRzwXguCFkGtQsW45cxKrzJLUcF4EP2fVMWKlvsVKyoQxYoKRLGiAlGsqEAUKyoQxYoKRLGiAlGsqECUaADgfxiJ2dZS66oVAAAAAElFTkSuQmCC"
        />
    </svg>  
}

export default Logo;
