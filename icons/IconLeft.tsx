import React from "react";

type Props = {
    color?: string;
} & React.SVGAttributes<any>;

const IconLeft: React.FC<Props> = ({ color, ...props }) => {
	return <svg xmlns="http://www.w3.org/2000/svg" width="21" height="31" viewBox="0 0 21 31">
        <path id="_" fill={color ?? "#efefef"} fillRule="evenodd" data-name="&lt;" d="M392.294,1746L377,1761.5l15.293,15.5,5.705-5.7-9.667-9.8,9.667-9.8Z" transform="translate(-377 -1746)"/>
    </svg>
}

export default IconLeft;
