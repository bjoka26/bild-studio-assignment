import React from "react";

type Props = {
    color?: string;
} & React.SVGAttributes<any>;

const IconRight: React.FC<Props> = ({ color, ...props }) => {
	return <svg xmlns="http://www.w3.org/2000/svg" width="21" height="31" viewBox="0 0 21 31">
        <path id="_" fill={color ?? "#efefef"} fillRule="evenodd" data-name="&lt;" d="M382.706,1777L398,1761.5,382.706,1746,377,1751.7l9.667,9.8L377,1771.3Z" transform="translate(-377 -1746)"/>
    </svg>

}

export default IconRight;
