import styled from "styled-components";
import GoogleMapReact from "google-map-react";

import Marker from "../icons/Marker";

const Container = styled.div({
    width: "100%",
    height: "500px",
});

export default () => {
    const defaultProps = {
        center: {
          lat: 42.4304,
          lng: 19.2594
        },
        zoom: 11
    };
    return <Container>
        <GoogleMapReact
            bootstrapURLKeys={{ key: process.env.NEXT_PUBLIC_GOOGLE_API_KEY as string }}
            defaultCenter={defaultProps.center}
            defaultZoom={defaultProps.zoom}
        >
            <Marker
                lat={42.4304}
                lng={19.2594}
                text="My Marker"
            />
        </GoogleMapReact>
    </Container>;
}
