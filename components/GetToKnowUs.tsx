import Image from "next/image";
import styled from "styled-components";
import Modal from "react-modal";
import { useState } from "react";
import Bounce from "react-reveal/Bounce";
import { Container } from "react-bootstrap";

import Button from "./Button";
import TextAndParagraph from "./TextAndParagraph";

const GetToKnowUsContainer = styled.div({
    margin: "4rem 0",
    background: "#efefef",
    padding: "3rem 0",
});

const InnerFlexbox = styled.div({
    display: "flex",
    flexDirection: "row",
    flexWrap: "nowrap",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    alignContent: "center",

    ".text-container": {
        marginTop: "0",
        marginLeft: "30px"
    },

    "@media only screen and (max-width: 992px)": {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",

        ".text-container": {
            marginTop: "50px",
        },
    }
});

const VideoPlayerPlaceholder = styled.div({
    position: "relative",
    cursor: "pointer",
    minWidth: "380px",
    height: "240px",

    "@media only screen and (max-width: 375px)": {
        minWidth: "100%"
    }
});

const ModalControls = styled.div({
    textAlign: "right",
    marginTop: "1.5rem",
});

const modalStyle = {
    overlay: {
        backdropFilter: "blur(10px)",
        backgroundColor: "rgba(0, 0, 0, .3)",
    },
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        borderRadius: "0",
    },
};

const VideoWrapper = styled.div({
    position: "relative",
    paddingBottom: "56.25%", /* 16:9 */
    paddingTop: "25px",
    height: "0",
});

const IFrame = styled.iframe({
    position: "absolute",
    top: "0",
    left: "0",
    width: "100%",
    height: "100%",
});

export default () => {
    const [videoModalOpen, setVideoModalOpen] = useState(false);

    const openModal = () => {
        setVideoModalOpen(true);
    }

    const closeModal = () => {
        setVideoModalOpen(false);
    }

    return <GetToKnowUsContainer>
        <Modal
            isOpen={videoModalOpen}
            onRequestClose={closeModal}
            style={modalStyle}
        >
            <Bounce top delay={250}>
                <h2>Instructions not clear, here's a Rick Roll!</h2>
            </Bounce>

            <VideoWrapper>
                <IFrame
                    src="https://www.youtube.com/embed/BBJa32lCaaY?autoplay=1"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                ></IFrame>
            </VideoWrapper>

            <ModalControls>
                <Button onClick={() => closeModal()}>Close modal</Button>
            </ModalControls>
        </Modal>

        <Container>
            <InnerFlexbox>
                <VideoPlayerPlaceholder onClick={() => openModal()}>
                    <Image src="/images/video-player-placeholder.png" fill alt="A picture of a video player" />
                </VideoPlayerPlaceholder>

                <TextAndParagraph alignText="start" margin="0">
                    <h4 className="mb-3">Get to know us a little better!</h4>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu erat lacus, vel congue mauris. Fusce velit justo, faucibus eu sagittis ac, gravida quis tortor. Suspendisse non urna mi, quis tincidunt eros. Nullam tellus turpis, fringilla sit amet congue ut, luctus a nulla. Donec sit amet sapien neque, id ullamcorper diam. Nulla facilisi. Pellentesque pellentesque arcu a elit congue lacinia.
                    </p>
                    <p>
                        Nullam tellus turpis, fringilla sit amet congue ut, luctus a nulla. Donec sit amet sapien neque, id ullamcorper diam. Nulla facilisi. Pellentesque pellentesque arcu a elit congue lacinia.
                    </p>
                </TextAndParagraph>
            </InnerFlexbox>
        </Container>
    </GetToKnowUsContainer>;
}
