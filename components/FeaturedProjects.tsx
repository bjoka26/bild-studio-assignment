import Slideshow, { Slide } from "./Slideshow";
import TextAndParagraph from "./TextAndParagraph";

const slides: Slide[] = [
    {
        image: "/images/project-jezik.jpg.png",
        title: "Project Jezik",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu erat lacus, vel congue mauris. Fusce velit justo, faucibus eu sagittis. 1",
        url: "#",
    },
    {
        image: "/images/project-boy-and-girl.jpg.png",
        title: "Projekt Boy and Girl",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu erat lacus, vel congue mauris. Fusce velit justo, faucibus eu sagittis. 2",
        url: "#",
    },
    {
        image: "/images/project-buttons.jpg.png",
        title: "Project Buttons",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu erat lacus, vel congue mauris. Fusce velit justo, faucibus eu sagittis. 3",
        url: "#",
    },
    {
        image: "/images/project-social-media.jpg.png",
        title: "Project Social Media",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu erat lacus, vel congue mauris. Fusce velit justo, faucibus eu sagittis. 4",
        url: "#",
    },
    {
        image: "/images/project-jes.jpg.png",
        title: "Projekt Jes",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu erat lacus, vel congue mauris. Fusce velit justo, faucibus eu sagittis. 5",
        url: "#",
    },
];

export default () => {
    return <>
            <TextAndParagraph>
                <h4>A couple of our featured projects</h4>

                <p className="mt-4">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu erat lacus, vel congue mauris. Fusce velit justo, faucibus eu sagittis ac, gravida quis tortor. Suspendisse non urna mi, quis tincidunt eros.
                </p>
            </TextAndParagraph>

        <Slideshow className="mt-5" slides={slides} />
    </>;
}
