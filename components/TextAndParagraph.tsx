import styled from "styled-components";

type Props = {
    alignText?: string;
    margin?: string;
} & React.AllHTMLAttributes<any>;

const TextAndParagraph: React.FC<Props> = ({ children, margin, alignText }: Props) => {

    const TextContainer = styled.div({
        textAlign: alignText ?? "center",
        marginTop: "50px",
    
        "h4": {
            color: "#8a8888",
            textTransform: "uppercase",
        },
    
        "p": {
            color: "#8a8888",
            margin: margin ?? "0 60px"
        },
    
        "@media only screen and (max-width: 768px)": {
            marginTop: "50px"
        },
    
        "@media only screen and (max-width: 425px)": {
            fontSize: ".875rem",

            "p": {
                margin: margin ?? "20px",
            }
        },
    });

    return <TextContainer className="text-container">
            {children}
    </TextContainer>;
}

export default TextAndParagraph;
