import Image from "next/image";
import { useEffect, useState } from "react";
import styled from "styled-components";
import { clone, cloneDeep, first, last } from "lodash";

import IconLeft from "../icons/IconLeft";
import IconRight from "../icons/IconRight";
import { Container } from "react-bootstrap";
import TextAndParagraph from "./TextAndParagraph";

export type Slide = {
    image: string;
    title: string;
    description: string;
    url: string;
};

type Props = {
    color?: string;
    slides: Slide[];
} & React.AllHTMLAttributes<any>;

enum SlideChangeDirection {
    PREVIOUS = 0,
    NEXT = 1,
};

const SlideshowContainer: any = styled.div({
    overflowX: "hidden",
});

const SlidesContainer = styled.div({
    display: "flex",
    flexDirection: "row",
    flexWrap: "nowrap",
    alignItems: "center",
    justifyContent: "space-evenly",
    alignContent: "center",
});

const SlideContainer = styled.div({
    margin: "0 1rem",
    width: "287px",
    height: "200px",
    position: "relative",
    textAlign: "center",
    cursor: "pointer",
    transition: ".2s all",

    "&:hover": {
        opacity: "1 !important",
        
        ".hover-icon": {
            opacity: "1",
        },
    },

    "&.ghosted": {
        opacity: ".5",
    },

    ".hover-icon": {
        zIndex: "100",
        marginTop: "-225px",
        opacity: "0",
        transition: ".2s all",
    }
});

const SlideDetailsAndControls = styled.div({
    margin: "4rem 15rem",
    display: "flex",
    flexDirection: "row",
    flexWrap: "nowrap",
    alignContent: "center",
    justifyContent: "space-around",
    alignItems: "center",

    "@media only screen and (max-width: 992px)": {
        margin: "2rem 7.5rem",
    },

    "@media only screen and (max-width: 768px)": {
        margin: "0rem 1.5rem",
    }
});

const SlideControl = styled.div({
    "svg path": {
        transition: ".2s all",
    },

    "&:hover": {
        cursor: "pointer",

        "svg path": {
            fill: "#2ecc71",
        },
    },
});

const Slideshow: React.FC<Props> = ({ color, slides, ...props }) => {
    const [currentSlideIndex, setCurrentSlideIndex] = useState(0);
    const [middleIndex, setMiddleIndex] = useState(0);
    const [internalSlides, setInternalSlides] = useState<Slide[]>([]);
    const [currentSlide, setCurrentSlide] = useState<Slide>({} as any);

    const isSlideInFocus = (slideId: number) => {
        return currentSlideIndex === slideId;
    }

    const changeSlide = (direction: SlideChangeDirection) => {
        switch (direction) {
            case SlideChangeDirection.PREVIOUS: {
                const newInternalSlides = clone(internalSlides);
                const firstSlide: Slide = first(newInternalSlides) as Slide;
                newInternalSlides.push(firstSlide);
                newInternalSlides.shift();
                
                setInternalSlides(newInternalSlides);
                break;
            }

            case SlideChangeDirection.NEXT: {
                const newInternalSlides = clone(internalSlides);
                const lastSlide: Slide = last(newInternalSlides) as Slide;
                newInternalSlides.unshift(lastSlide);
                newInternalSlides.pop();

                setInternalSlides(newInternalSlides);
                break;
            }
        }
    }

    useEffect(() => {
        setCurrentSlide(internalSlides[middleIndex]);
    }, [internalSlides]);

    useEffect(() => {
        const totalSlides: number = slides.length;

        if (totalSlides % 2 === 0) {
            setMiddleIndex(totalSlides / 2);
        } else {
            const remainder: number = Math.floor(totalSlides / 2);

            setMiddleIndex(totalSlides - remainder - 1);
        }

        setInternalSlides(cloneDeep(slides));
    }, [slides]);

    useEffect(() => {
        setCurrentSlideIndex(middleIndex);
        setCurrentSlide(internalSlides[middleIndex]);
    }, [middleIndex]);

    return <SlideshowContainer {...props}>
        <SlidesContainer>
            {
                internalSlides && internalSlides.map((slide: Slide, index: number) => {
                    return <SlideContainer className={isSlideInFocus(index) ? '' : 'ghosted'} key={`slide${index}`}>
                        <a href={slide.url}>
                            <Image src={slide.image} width={287} height={200} alt={slide.title} />
                            <Image src="/images/slide-link-hover.png" className="hover-icon" width={97} height={97} alt="Image hover icon" />
                        </a>
                    </SlideContainer>;
                })
            }
        </SlidesContainer>

        <Container>
            <SlideDetailsAndControls>
                <SlideControl onClick={() => changeSlide(SlideChangeDirection.PREVIOUS)}>
                    <IconLeft />
                </SlideControl>

                {
                    currentSlide && <TextAndParagraph>
                        <h4 className="mb-4">{ currentSlide.title }</h4>

                        <p>{ currentSlide.description }</p>
                    </TextAndParagraph>
                }

                <SlideControl onClick={() => changeSlide(SlideChangeDirection.NEXT)}>
                    <IconRight />
                </SlideControl>
            </SlideDetailsAndControls>
        </Container>
    </SlideshowContainer>;
}

export default Slideshow;
