import styled from "styled-components";
import { useCallback, useState } from "react";
import { GoogleReCaptchaProvider, GoogleReCaptcha } from 'react-google-recaptcha-v3';

import { FormData } from "../types/Form";
import Button from "./Button";

const Form = styled.form({
    display: "flex",
    flexDirection: "column",
    flexWrap: "nowrap",
    alignContent: "center",
    justifyContent: "center",
    alignItems: "flex-start",
    marginTop: "30px",
});

const Input = styled.input({
    width: "350px",
    height: "45px",
    backgroundColor: "#ffffff",
    border: "2px solid #8a8888",
    marginBottom: "15px",
    paddingLeft: "15px",
    color: "#737373",

    "&:focus": {
        outline: "none",
    },

    "@media only screen and (max-width: 992px)": {
        maxWidth: "100%",
    },
});

const TextArea = styled.textarea({
    width: "100%",
    height: "221px",
    backgroundColor: "#ffffff",
    border: "2px solid #8a8888",
    marginBottom: "15px",
    paddingLeft: "15px",
    color: "#737373"
});

export default () => {
    const [formData, setFormData] = useState<FormData>({
        name: "",
        email: "",
        subject: "",
        message: "",
    });
    const [emailError, setEmailError] = useState(false);
    const [token, setToken] = useState("");

    const handleInputs = (e: React.ChangeEvent<HTMLInputElement> | React.ChangeEvent<HTMLTextAreaElement>) => {
        const newData = {...formData};

        newData[e.currentTarget.name as keyof FormData] = e.currentTarget.value;

        setFormData(newData);
    };

    const validateEmail = (email: string) => {

        return email.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i);
    };

    const validateInputs = (inputType: string, inputValue: string): boolean => {
        if (inputType === "email" && validateEmail(inputValue) === null) return false;
        if (inputType === "message" && inputValue.length > 1000) {
            setEmailError(true);
            
            return false;
        }
        setEmailError(false);

        return true;
    };

    const verify = useCallback((token: string) => {           
        setToken(token);
      }, []);

    const submitButton = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        for (const [key, value] of Object.entries(formData)) {
            validateInputs(key, value);
        }
    };

    return <Form onSubmit={submitButton}>
        <Input type="text" placeholder="Name" required name="name" onChange={(e) => handleInputs(e)} />
        <Input type="text" placeholder="Email Address" required name="email" onChange={(e) => handleInputs(e)} />
        <Input type="text" placeholder="Subject" required name="subject" onChange={(e) => handleInputs(e)} />
        <TextArea name="message" required={emailError} onChange={(e) => handleInputs(e)}></TextArea>
        <GoogleReCaptchaProvider reCaptchaKey={ process.env.NEXT_PUBLIC_RECAPTCHA_API_KEY as string }>
            <GoogleReCaptcha onVerify={verify} />
        </GoogleReCaptchaProvider>
        <Button>Send Message</Button>
    </Form>;
}
