import Image from "next/image";
import React from "react";
import styled from "styled-components";

import { Project } from "../types/Project";

type Props = {
    projects: Project[];
} & React.AllHTMLAttributes<any>;

const ProjectsGridContainer = styled.div({
    display: "grid",
    gridTemplateRows: "repeat(3, 1fr)",
    gridTemplateColumns: "repeat(3, 1fr)",
    gap: "2rem",
    height: "100%",

    "@media only screen and (max-width: 1024px)": {
        gridTemplateColumns: "repeat(2, 1fr)",
    },

    "@media only screen and (max-width: 768px)": {
        gridTemplateColumns: "repeat(1, 1fr)",
    }
});

const NoData = styled.div({
    display: "grid",
    gridTemplateRows: "repeat(3, 1fr)",
    textAlign: "center",
    gridColumn: "2 / 2",
    fontSize: "1.2rem",
    fontWeight: "bold",
    color: "#8a8888",
});

const ProjectGrid = styled.a({
    textAlign: "center",
    position: "relative",
    cursor: "pointer",
    textDecoration: "none",

    "&:hover": {
        ".hover-icon": {
            opacity: "1",
        }
    },

    ".hover-icon": {
        position: "absolute",
        inset: "0",
        margin: "auto",
        transition: ".2s all",
        opacity: "0",
    },
});

const ProjectsGridView: React.FC<Props> = ({ projects, ...props }) => {
	return <ProjectsGridContainer>
        {
            projects.length ? projects.map((project: Project, index) => {
                return <ProjectGrid href={project.url} key={`gridproject${index}`}>
                    <Image src={project.image} width={300} height={220} alt="Project image" />
                    <Image src="/images/slide-link-hover.png" className="hover-icon" width={97} height={97} alt="Image hover icon" />
                </ProjectGrid>
            }) : 
            <NoData>
                No Results
            </NoData>
        }
    </ProjectsGridContainer>
}

export default ProjectsGridView;
