import { useState } from "react";
import styled from "styled-components";
import { Container } from "react-bootstrap";
import Bounce from "react-reveal/Bounce";
import Modal from "react-modal";

import Button from "./Button";
import { menuItems } from "../Constants";

const FooterContainer = styled.footer({
    background: "#322f2f",
    height: "205px",
});

const FooterCopyright = styled.div({
    height: "64px",
    background: "#efefef",

    "@media only screen and (max-width: 768px)": {
        height: "fit-content",
        paddingBottom: "10px",
    }
});

const FooterCopyrightGrid = styled.div({
    display: "flex",
    flexDirection: "row",
    flexWrap: "nowrap",
    alignContent: "center",
    justifyContent: "space-between",
    alignItems: "center",

    "@media only screen and (max-width: 768px)": {
        flexDirection: "column",
    }
});

const CopyrightText = styled.p({
    margin: "0",
    color: "#a5a5a5",
    lineHeight: "64px",
    textTransform: "uppercase",
    fontSize: ".85rem",
    fontWeight: "600",

    "@media only screen and (max-width: 375px)": {
        fontSize: ".75rem",
        lineHeight: "34px",
    }
});

const BottomMenu = styled.div({
    "@media only screen and (max-width: 375px)": {
        fontSize: ".75rem",
    }
});

const BlownAwayContainer = styled.div({
    display: "flex",
    flexDirection: "row",
    flexWrap: "nowrap",
    alignContent: "space-around",
    justifyContent: "space-between",
    alignItems: "center",
    height: "141px",

    "h4": {
        color: "#efefef",
        lineHeight: "141px",
        textTransform: "uppercase",
        letterSpacing: "1px",
    },
    "@media only screen and (max-width: 768px)": {
        "h4": {
            lineHeight: "30px",
        },
        "button": {
            fontSize: "0.875rem",
        },
    },
    "@media only screen and (max-width: 375px)": {
        "button, h4": {
            fontSize: ".75rem",
            lineHeight: "24px",
        }
    }
});

const ModalControls = styled.div({
    textAlign: "right",
    marginTop: "1.5rem",
});

const modalStyle = {
    overlay: {
        backdropFilter: "blur(10px)",
        backgroundColor: "rgba(0, 0, 0, .3)",
    },
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        borderRadius: "0",
    },
};

const BottomMenuItem = styled.a({
    textDecoration: "none",
    color: "#737373",
    transition: ".2s color",
    background: "url('./images/slash.png') no-repeat",
    backgroundPosition: "right",
    paddingRight: "1.4rem",

    "&:nth-last-of-type(1)": {
        background: "transparent",
    },

    "&.active": {
        color: "#2ecc71",
        fontWeight: "bold",
    },

    "&:hover": {
        color: "#2ecc71",
    },
});

const Footer: React.FC<React.SVGAttributes<any>> = ({ ...props }) => {
    const [videoModalOpen, setVideoModalOpen] = useState(false);

    const openModal = () => {
        setVideoModalOpen(true);
    }

    const closeModal = () => {
        setVideoModalOpen(false);
    }

    return <FooterContainer {...props}>
        <Modal
            isOpen={videoModalOpen}
            onRequestClose={closeModal}
            style={modalStyle}
        >
            <Bounce top delay={250}>
                <h2>Instructions not clear, here's a Rick Roll!</h2>
            </Bounce>

            <iframe
                width="1280"
                height="720"
                src="https://www.youtube.com/embed/BBJa32lCaaY?autoplay=1"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            ></iframe>

            <ModalControls>
                <Button onClick={() => closeModal()}>Close modal</Button>
            </ModalControls>
        </Modal>

        <Container>
            <BlownAwayContainer>
                <h4>Are You Ready To Be Blown Away?</h4>

                <Button onClick={() => openModal()}>Click here to find out</Button>
            </BlownAwayContainer>
        </Container>

        <FooterCopyright>
            <Container>
                <FooterCopyrightGrid>
                    <CopyrightText>Copyright 2013 Display. All Rights Reserved.</CopyrightText>

                    <BottomMenu>
                        {
                            menuItems && menuItems.map((item, index) => {
                                return <BottomMenuItem href={item.url} key={`bottommenuitem${index}`}>{item.label}</BottomMenuItem>
                            })
                        }
                    </BottomMenu>
                </FooterCopyrightGrid>
            </Container>
        </FooterCopyright>
    </FooterContainer>;
}

export default Footer;
