import { useState } from "react";
import styled from "styled-components";

import Dribble from "../icons/Dribble";
import Facebook from "../icons/Facebook";
import GooglePlus from "../icons/GooglePlus";
import Pinterest from "../icons/Pinterest";
import Rss from "../icons/Rss";
import Twitter from "../icons/Twitter";

const socialMediaIcons = [
    {
        component: <Twitter />,
        hoverColor: "#1DA1F2",
        url: "#",
    },
    {
        component: <Facebook />,
        hoverColor: "#4267B2",
        url: "#",
    },
    {
        component: <Rss />,
        hoverColor: "#ee802f",
        url: "#",
    },
    {
        component: <Pinterest />,
        hoverColor: "#E60023",
        url: "#",
    },
    {
        component: <GooglePlus />,
        hoverColor: "#db4a39",
        url: "#",
    },
    {
        component: <Dribble />,
        hoverColor: "#ea4c89",
        url: "#",
    },
];

const Icon = styled.a({
    marginLeft: "9px",
});

export default () => {
    const [hoveredIconIndex, setHoveredIconIndex] = useState(-1);

    const onHoverEnter = (index: number) => {
        setHoveredIconIndex(index);
    };

    const onHoverExit = () => {
        setHoveredIconIndex(-1);
    };

    return <div className="social-media">
        {
            socialMediaIcons && socialMediaIcons.map((icon, index) => {
                const Component = icon.component.type;
                return (<Icon href={icon.url} onMouseEnter={() => onHoverEnter(index)} onMouseLeave={() => onHoverExit()} key={`icon${index}`}>
                    <Component key={ index } color={index === hoveredIconIndex ? icon.hoverColor : undefined} />
                </Icon>);
            })
        }
    </div>
}
