import Link from "next/link";
import styled from "styled-components";
import Fade from "react-reveal/Fade";
import { Container } from "react-bootstrap";

import Button from "./Button";

const HeroDescriptionContainer = styled.div({
    margin: "4rem 0",
});

const Description = styled.p({
    textAlign: "center",
    color: "#b3b2b2",
    fontWeight: "600",
    padding: "0 12rem",
    fontSize: "1.2rem",
    textTransform: "uppercase",

    "@media only screen and (max-width: 768px)": {
        padding: "0 3rem",
        fontSize: "1rem",
    },

    "@media only screen and (max-width: 425px)": {
        fontSize: "1rem",
    },

    "@media only screen and (max-width: 375px)": {
        fontSize: ".875rem",
        margin: "0",
    }
});

export default () => {
    return <HeroDescriptionContainer>
        <Container className="text-center">
            <Fade top delay={250}>
                <Description>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu eratiuy lacus, vel congue mauris. Fusce velitaria justo, faucibus eu.
                </Description>
            </Fade>

            <Fade top delay={400}>
                <Link href="/work">
                    <Button className="mt-3">Browse portfolio</Button>
                </Link>
            </Fade>
        </Container>
    </HeroDescriptionContainer>;
}
