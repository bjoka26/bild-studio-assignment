import Image from "next/image";
import React from "react";
import styled from "styled-components";

import { Project } from "../types/Project";

type Props = {
    projects: Project[];
} & React.AllHTMLAttributes<any>;

const ProjectsListContainer = styled.div({
    display: "grid",
    gridTemplateRows: "repeat(4, 1fr)",
    height: "100%",
});

const NoData = styled.div({
    display: "grid",
    gridTemplateRows: "repeat(3, 1fr)",
    textAlign: "center",
    fontSize: "1.2rem",
    fontWeight: "bold",
    color: "#8a8888",
});

const ProjectList = styled.a({
    display: "flex",
    marginBottom: "1rem",
    cursor: "pointer",
    transition: ".2s all",
    padding: "1rem",
    textDecoration: "none",

    "&:hover": {
        background: "rgba(0, 0, 0, .1)",
        transition: "0s all",
    },

    "img": {
        objectFit: "contain",
    },

    "@media only screen and (max-width: 550px)": {
        flexDirection: "column",
        alignItems: "center",
    }
});

const ProjectDetails = styled.div({
    "h4": {
        color: "#8a8888",
        fontWeight: "bold",
    },

    "p": {
        color: "#6a6a6a",
    },

    "@media only screen and (max-width: 550px)": {
        margin: "20px",
    },
});

const ProjectsListView: React.FC<Props> = ({ projects, ...props }) => {
	return <ProjectsListContainer>
        {
            projects.length > 0 ? projects.map((project: Project, index) => {
                return <ProjectList href={project.url} key={`listproject${index}`}>
                    <Image className="me-3" src={project.image} width={200} height={120} alt="Project image" />
                    <ProjectDetails>
                        <h4>{project.title}</h4>
                        <p>{project.description}</p>
                    </ProjectDetails>
                </ProjectList>
            }) : 
            <NoData>
                No Results
            </NoData>
        }
    </ProjectsListContainer>
}

export default ProjectsListView;
