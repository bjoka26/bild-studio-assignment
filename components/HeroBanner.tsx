import Image from "next/image";
import styled from "styled-components";
import Fade from "react-reveal/Fade";

const HeroScreenContainer = styled.div({
    position: "relative",
});

const Sunburst = styled.div({
    backgroundImage: "url('./images/sunburst.png')",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    height: "527px",
    marginTop: "-527px",
    width: "100%",
    top: "0",
    zIndex: "9",
});

const Devices = styled.div({
    position: "relative",
    height: "472px",
    minWidth: "830px",
    zIndex: "10",
    margin: "auto",
    marginTop: "-400px",
    userSelect: "none",

    "@media only screen and (min-width: 835px)": {
        width: "830px"
    },

    "@media only screen and (max-width: 835px)": {
        minWidth: "100%"
    },

    "@media only screen and (max-width: 550px)": {
        marginTop: "-300px",
        minWidth: "30%",
        height: "320px"
    },

    "@media only screen and (max-width: 425px)": {
        marginTop: "-280px",
        height: "280px"
    },

    "@media only screen and (max-width: 320px)": {
        marginTop: "-250px",
        height: "250px"
    },
});

const HeroBannerGreenscreen = styled.div({
    background: "#2ecc71",
    height: "527px",
});

export default () => {
    return <HeroScreenContainer>
        <HeroBannerGreenscreen>
            
        </HeroBannerGreenscreen>

        <Sunburst />

        <Fade big>
            <Devices>
                <Image 
                    src="/images/slider-overimage.png"
                    fill
                    alt="Image of devices"
               />
            </Devices>
        </Fade>
    </HeroScreenContainer>;
}
