import styled from "styled-components";

const AppButton = styled.button({
    border: "0",
    padding: ".75rem 1.5rem",
    backgroundColor: "#2dcb71",
    color: "white",
    fontWeight: "bold",
    textTransform: "uppercase",
    transition: ".2s background",

    "&:hover": {
        background: "#12a350",
    }
});

const Button: React.FC<React.ButtonHTMLAttributes<any>> = ({ children, ...props }) => {
    return <AppButton className="app-btn" {...props}> { children } </AppButton>;
}

export default Button;
