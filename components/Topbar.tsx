import { useRouter } from "next/router";
import { Container } from "react-bootstrap";
import styled from "styled-components";
import { menuItems } from "../Constants";

import Logo from "../icons/Logo";
import SocialMediaIcons from "./SocialMediaIcons";

const Topbar = styled.div({
    height: "195px",
});

const Brandings = styled.div({
    height: "111px",
    display: "flex",
    flexDirection: "row",
    flexWrap: "nowrap",
    alignContent: "center",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottom: "1px solid #dadada",
});

const MenuItems = styled.div({
    margin: "30px 0",
});

const MenuItem = styled.a({
    textDecoration: "none",
    marginRight: "38px",
    color: "#737373",
    transition: ".2s color",

    "&.active": {
        color: "#2ecc71",
        fontWeight: "bold",
    },

    "&:hover": {
        color: "#2ecc71",
    },

    "@media only screen and (max-width: 375px)": {
        marginRight: "15px",
        fontSize: "0.85rem",
    },
});

export default () => {
    const router = useRouter();

    const isPathActive = (path: string) => {
        return router.asPath === path;
    }

    return <Topbar>
        <Container>
            <Brandings>
                <Logo />

                <SocialMediaIcons />
            </Brandings>

            <MenuItems>
                {
                    menuItems && menuItems.map((item, index) => {
                        return <MenuItem href={item.url} className={isPathActive(item.url) ? "active" : ""} key={`mitem${index}`}>{item.label}</MenuItem>;
                    })
                }
            </MenuItems>
        </Container>
    </Topbar>;
}
