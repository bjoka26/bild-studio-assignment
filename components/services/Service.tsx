import styled from "styled-components";

import { ServiceType } from "../../types/ServiceType";

type ServiceProps = {
    index: number;
    service: ServiceType;
    selectService: number;
    handleServiceTabChange: () => void;
}

const ServiceName = styled.h3({
    textTransform: "uppercase",
    fontWeight: "bold",
    fontSize: "1.125rem",
    marginTop: "30px",
});

const IconBox = styled.div({
    width: "58px",
    height: "52px"
});

export default ({ service, handleServiceTabChange, index, selectService }: ServiceProps) => {
    const ServiceBox = styled.div({
        width: "240px",
        height: "200px",
        display: "flex",
        flexDirection: "column",
        flexWrap: "nowrap",
        alignContent: "center",
        justifyContent: "center",
        alignItems: "center",
        cursor: "pointer",
        color: index === selectService && "#423d3d",
        backgroundColor: index === selectService && "#dadada",
        
        "&:hover": {
            backgroundColor: "#dadada",
            color: "#423d3d",

            ".service-icon": {
                fill: "#423d3d",
            }
        }
    } as any);

    const Icon = service.icon.type;    

    return <ServiceBox onClick={handleServiceTabChange}>
        <IconBox>
            <Icon color={index === selectService ? "#423d3d" : null} />
        </IconBox>
        <ServiceName>{service.name}</ServiceName>
    </ServiceBox>;
};
