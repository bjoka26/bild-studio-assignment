import { useState } from "react";
import styled from "styled-components";

import Arrow from "../../icons/Arrow";
import { ServiceType } from "../../types/ServiceType";
import Service from "./Service";
import Wheels from "../../icons/Wheels";
import Camera from "../../icons/Camera";
import MagnifyingGlass from "../../icons/MagnifyingGlass";
import Application from "../../icons/Application";

const ServiceList = styled.div({
    backgroundColor: "#efefef",
    height: "200px",
    marginBottom: "30px",
    "@media only screen and (max-width: 768px)": {
        height: "fit-content"
    }
});

const Container = styled.div({
    height: "inherit",
    display: "flex",
    flexDirection: "row",
    flexWrap: "nowrap",
    alignContent: "center",
    justifyContent: "center",
    alignItems: "center",
    "@media only screen and (max-width: 768px)": {
        flexWrap: "wrap",
    }
});

const ServiceContainer = styled.div({
    color: "#8a8888",
    margin: "50px 0 50px 0"
});

const Items = styled.p({
    fontSize: "0.875rem",
    display: "flex",
    flexDirection: "row",
    flexWrap: "nowrap",
    alignContent: "center",
    justifyContent: "flex-start",
    alignItems: "center",
    marginBottom: "15px"
});

const Title = styled.h3({
    fontSize: "1.5rem",
    fontWeight: "bold",
    textTransform: "uppercase",
    marginBottom: "30px"
});

export default () => {
    const [selectService, setSelectService] = useState(0);
    const services: ServiceType[] = [
        {
            icon: <Wheels />,
            name: 'Website',
            text: '1Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu erat lacus, vel congue mauris. Fusce velit justo, faucibus eu sagittis ac, gravida quis tortor. Suspendisse non urna mi, quis tincidunt eros. Nullam tellus turpis, fringilla sit amet congue ut, luctus a nulla. Donec sit amet sapien neque, id ullamcorper diam. Nulla facilisi. Pellentesque pellentesque arcu a elit congue lacinia.',
            list: [
                {
                    text: "1Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                },
                {
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit fringilla sit amet.",
                },
                {
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit luctus donec.",
                }
            ],
        },
        {
            icon: <Camera />,
            name: 'Photography',
            text: '2Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu erat lacus, vel congue mauris. Fusce velit justo, faucibus eu sagittis ac, gravida quis tortor. Suspendisse non urna mi, quis tincidunt eros. Nullam tellus turpis, fringilla sit amet congue ut, luctus a nulla. Donec sit amet sapien neque, id ullamcorper diam. Nulla facilisi. Pellentesque pellentesque arcu a elit congue lacinia.',
            list: [
                {
                    text: "2Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                },
                {
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit fringilla sit amet.",
                },
                {
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit luctus donec.",
                }
            ],
        },
        {
            icon: <MagnifyingGlass />,
            name: 'Seo',
            text: '3Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu erat lacus, vel congue mauris. Fusce velit justo, faucibus eu sagittis ac, gravida quis tortor. Suspendisse non urna mi, quis tincidunt eros. Nullam tellus turpis, fringilla sit amet congue ut, luctus a nulla. Donec sit amet sapien neque, id ullamcorper diam. Nulla facilisi. Pellentesque pellentesque arcu a elit congue lacinia.',
            list: [
                {
                    text: "3Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                },
                {
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit fringilla sit amet.",
                },
                {
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit luctus donec.",
                }
            ],
        },
        {
            icon: <Application />,
            name: 'Applications',
            text: '4Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu erat lacus, vel congue mauris. Fusce velit justo, faucibus eu sagittis ac, gravida quis tortor. Suspendisse non urna mi, quis tincidunt eros. Nullam tellus turpis, fringilla sit amet congue ut, luctus a nulla. Donec sit amet sapien neque, id ullamcorper diam. Nulla facilisi. Pellentesque pellentesque arcu a elit congue lacinia.',
            list: [
                {
                    text: "4Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                },
                {
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit fringilla sit amet.",
                },
                {
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit luctus donec.",
                }
            ],
        }
    ];

    const handleServiceTabChange = (id: number): void => {
        console.log(id);
        
        setSelectService(id);
    }

    return <div className="services">
        <ServiceContainer>
            <div className="container">
                <Title>Services</Title>
            </div>
            <ServiceList>
                <Container className="container">
                    {
                        services.map((service: ServiceType, index) => {                                                        
                            return <Service selectService={selectService} handleServiceTabChange={() => handleServiceTabChange(index)} index={index} service={service}  key={`service${index}`} />
                        })
                    }
                </Container>
            </ServiceList>
            <div className="container">
                <p className="text">{services[selectService]?.text}</p>
                {
                    services[selectService].list.map((list, index) => {
                        return <Items key={`arservise${index}`}> <Arrow color="#413d3d" /> { list.text } </Items>
                    })
                }
            </div>
        </ServiceContainer>
    </div>
};
