import styled from "styled-components";

const HeroTitleContainer = styled.div({
    background: "#2ecc71",
    height: "100px",
    color: "#fff",
    textTransform: "uppercase",
    fontWeight: "bold",
    lineHeight: "100px",
    fontSize: "1.7rem",

    "@media only screen and (max-width: 768px)": {
        fontSize: "1.3rem",
    },

    "@media only screen and (max-width: 320px)": {
        fontSize: "0.875rem",
    },
}) as any;

const BigHeroTitle: React.FC<React.AllHTMLAttributes<any>> = ({ children, ...props }) => {
    return <HeroTitleContainer {...props}>{ children }</HeroTitleContainer>;
}

export default BigHeroTitle;
